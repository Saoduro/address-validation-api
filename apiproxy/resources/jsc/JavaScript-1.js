var dConfirmed = context.getVariable("soapresponse.deliveryConfirmed");
var valid = context.getVariable("soapresponse.valid");
var couldNotValidate = context.getVariable("soapresponse.couldNotValidate");
var scode = context.getVariable("soapresponse.scode");
var line2 = context.getVariable("soapresponse.line2");
if (scode != null && couldNotValidate != null) {
        context.setVariable("soapresponse.scode", scode + ":" + couldNotValidate);
}
if (valid === null) {
    context.setVariable("soapresponse.valid", "Y");
} 
if (dConfirmed != 'Y') {
    context.setVariable("soapresponse.deliveryConfirmed", 'N')
}
if(line2 == "NULL") {
    context.setVariable("soapresponse.line2","");
}